//
//  main.cpp
//  Bisection
//
//  Created by Marta Rusin on 05/11/2022.
//

#include <iostream>
using namespace std;
double bisection(double, double, double);

int main() {
    double a;
    double b;
    double eps;
    
    //Sprawdzic czy w podanym przedziale jest miejsce zerowe (f(a)*f(b) <=0)

    cout << "Podaj a: " <<endl;
    cin >> a;
    cout << "Podaj b: " <<endl;
    cin >> b;
    cout << "Podaj eps: " <<endl;
    cin >> eps;
    
    cout << "Miejsce zerowe: " << bisection(a, b, eps) << endl;
    
    
    
    //Dane testowe: a = -10, b=83, eps = 0.01, wynik ok 5
    return 0;
}

double f(double x) {
    return x-5;
}

double bisection(double a, double b, double eps) {
    //W03, str 3

    double result;
    double s;
    
    while ( (b-a) > eps) {
   
        s=(a+b)/2;
        
        if(f(s) == 0) {
            result = s;
            break;
        }
        
        if(f(a)*f(s)<0) {
            b=s;
        }
        if(f(b)*f(s)<0) {
            a=s;
        }
    }
    result = s;
    return result;
}
